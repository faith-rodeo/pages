---
layout: post.liquid

title: Solidarity Need Not Apply
is_draft: false
published_date: 2022-04-17 12:00:00 +0100
data:
    author: <span class="tag-water">water</span>
    date: 2022-04-18
---
Your college has a housing problem. The city is growing, and people are getting admitted in droves. Understandably, the college offers a first-year residence guarantee to make sure all the in-bound students have a place to live in the fall. However, residence for upper-year students is extremely limited. Excluding the lucky few who find a spot-on campus, most returning students find housing off-campus and travel in the mornings.<span data-separator></span>

As a consequence of speculative real-estate trading, you can't seem to find a studio apartment within the city limits for less than 4000$ per month. Some entrepreneurially-minded landlords have been investing in land surrounding the student slums (all of which are also tied up in longer-term contracts) to build cheap (relatively speaking) housing for students. You, with your go-getter attitude, were fortunate enough to find a small (read: cozy) place to live before they were all bought up. The rent was still quite a bit more than you could budget, but even more fortunately, you convinced your landlord to let you sign a few of your friends' names to the lease. You knew that it'd be difficult, but unlike most students this season, you had housing security.

Shortly after you signed the lease, one of your roommates sent you an article published by the local newspaper about living conditions in the student slums. You discovered that, while not particularly surprising, women have repeatedly reported being stalked, harassed and sent threats while travelling to and from the school. Further, the paper found that 34 break-ins were reported in the last year alone, and only four were investigated. Your college is generally a safe place, and there are preventative measures to ensure that everyone gets the support they need while on campus. However, the landlords changed their policies. Previously, there was an implicit agreement between the four companies managing different apartment blocks in the area to only sign leases to students currently attending college. For various reasons--perhaps a quirk in supply and demand one year--one of the companies decided to start leasing rooms to non-students. The following year, 70% of students reported that they didn't feel safe when going to and from school.

This worries you. You recognized that you were going to be in danger in your own home. Though, you found the fact that you were worried a little worrying as well. When you stopped and think critically about the problem, you realized that this "problem" is implicitly classist. There was a hidden assumption in the argument presented by the journalist who broke the story. By your city's standards, the property in the student slums is low-income housing. After it stopped being student-only housing, that's exactly what it became, and that's what it was marketed as. The dangerous people who were encroaching on this neighbourhood were just poor people. It makes a lot of sense why people who had spent most of their lives in white, suburban neighbourhoods would get anxious when finding themselves suddenly surrounded by people they identify as poor. They've spent their entire lives being told that poor people are dangerous. It's no surprise that people who've never seen a drug deal in their lives would worry when they see substances trading hands. They're witnessing the real world for the first time, and they don't like it.

What about the stalking, harassment and break-ins? Your knee-jerk response is that the solution must be to bring in more police, but you stop, take a step back, and use your realization to try and develop a more preventative, communal solution. Perhaps you know from experience that calling the police doesn't always make people safer. Having had a few brushes with poverty yourself, you might realize that the problem is never "poor people," but rather financial insecurity itself that leads people into bad situations. So, perhaps you should talk with your neighbours and organize a renter's union and demand the rental oligopoly lower its prices. This would provide support for those who need it the most. Further, it would get more people involved in the community, forcing you and your neighbours to see each other as people and not loud noises in the middle of the night.

This doesn't entirely solve the problem of harassment, however. Still, you're adamant that police aren't the solution. In your Indigenous Studies class, you heard about Aboriginal neighbourhood watch programs that emphasize prevention and conflict resolution over protecting private property. Perhaps, you think, you could recreate such a program in your own neighbourhood, having volunteers walk with people who feel unsafe at night, keeping an eye out for others who need support.

You've covered a lot of ground, and your optimistic that with the right help, you and your friends could make a real difference. Excited, you tell all of your ideas to one of your roommates. You've thought about this problem quite a bit, and it's nice to get the ideas out of your head. Your roommate, however, seems disinterested. They tell you that you're worrying too much.

"I have a car. You can just carpool with me"

You're a little taken aback by this comment. They're right, of course, but that doesn't really solve the problem; it just takes you and your roommate out of the equation. Further, having a car is not a privilege everyone in your apartment block can afford. You're about to tell them this, but then they continue:

"Besides, you're not going to need to worry about being harassed anyways."

You understand what they mean. For the better or the worse, when dangerous people see you walking down the street at night, they aren't going to see a woman.

Later that year, as you're leaving your apartment with your roommate, you notice two of your neighbours---two middle-aged men---walking ahead of you. You've seen them around quite a bit. They're the kind of people the college students on your floor try to avoid. You've thought about talking to them a few times. You suspect they might be the lefty-types; they're the kind of people you would have needed to talk to if you ever wanted to make those ideas you had months ago into a reality. But they don't know your pronouns, and these days, you have enough things to worry about as it is.

You do, however, overhear them talking about a local tech start-up that just scored a contract with the city to build "emergency stations" in low-income neighbourhoods. These emergency stations would have buttons that, when pressed, would immediately contact the police and send them to your location. You're not entirely sure how useful those would actually be, but you suspect that the officials are a lot more interested in bolstering the city's image as the tech capital of your country than it is in investing in projects that make logical sense.

Fortunately, you won't have to worry about it, because you can just carpool. You think you've heard that it's better for the environment, too.
